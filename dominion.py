#!/usr/bin/python3
# coding: utf8
#
# Tirage aléatoire des cartes Action du jeux Dominion (édité par Filosofia) pour constituer la réserve
#
# Cartes du jeux de base, ne prend pas en compte les extensions
#
# On considère que l'on doit avoir au moins une carte de chaque valeur, mais pas plus de quatre. 
# Ajout d'une condition supplémentaire sur les cartes fortes (valeur 4 ou 5) : pas plus de 7 au total.
#

import random

#tableau de tuple (Nom de la carte, valeur de la carte)
cartes = [
("Salle du trône",4),
("Chapelle",2),
("Bûcheron",3),
("Laboratoire",5),
("Forgeron",4),
("Milice",4),
("Sorcière",5),
("Jardin",4),
("Festival",5),
("Espion",4),
("Voleur",4),
("Festin",4),
("Douve",2),
("Cave",2),
("Bibliothèque",5),
("Mine",5),
("Prêteur sur gage",4),
("Marché",5),
("Chancelier",3),
("Chambre du conseil",5),
("Rénovation",4),
("Bureaucrate",4),
("Village",3),
("Atelier",3),
("Aventurier",5) # compté parmis les cartes à valeur 5
]

nb_by_val = [0,0,0,0,0,0]

NB_TIRAGE=10
NB_MAX_VALEUR=4 # Nombre max de carte par valeur
MAX_CARTE_FORTE=7 # Nombre max de carte de valeur 4 ou 5

random.seed()

def tirage(nb_tirage, nb_max_val):
    nb = 0
    carte_tirage = []
    while nb < nb_tirage:
        c = random.randrange(len(cartes))
        carte = cartes[c]
        val = carte[1]
        if ((nb_by_val[val]+1) <= nb_max_val and not ( (val==4 or val==5) and (nb_by_val[4]+nb_by_val[5]) >= MAX_CARTE_FORTE ) ):
            nb_by_val[val] += 1
            nb += 1
            cartes.pop(c)
            carte_tirage.append(carte)
    return carte_tirage

# tire 1 carte pour chaque valeur
carte_tirage = tirage(4, 1)

# tire le reste des cartes
carte_tirage.extend(tirage(NB_TIRAGE-4, NB_MAX_VALEUR))

for c in sorted(carte_tirage,key=lambda c:c[0]):
    print(c[0])

