import http.client
from bs4 import BeautifulSoup

SERVER="www.infoclimat.fr"
API_VIGNETTE="/infoclimat/vignette_fiche_temps.php?s=%s"

def get_temps_in_vignette(vignette):
  s = BeautifulSoup(vignette,"lxml")
  return float(s.find("b",class_="vignette-mini-temperature").text[:-2])

def get_vignette_station(station_code):
  conn = http.client.HTTPSConnection(SERVER)

  conn.request("GET", API_VIGNETTE % (station_code))
  
  response = conn.getresponse()
  
  return response.read().decode()
  
  
v = get_vignette_station("000CR")
print (get_temps_in_vignette(v))
