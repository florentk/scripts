for i in *
do
	if [ -d "$i" ]; then
		cd "$i"
		ebook=""
		if [ -d "OEBPS" ]; then
			rm *.rar
			dst="${i%.*}.epub"
			zip -r "$dst" * 
			ebook="$dst"
		else
			#echo ------------ --------------
			#ls
			for p in *.epub
	        	do
				if [ -f "$p"  ] ; then
				  ebook="$p"
				  break	
				fi
			done

			for p in *.pdf
	        	do
				if [ -f "$p"  ] ; then
				  ebook="$p"
				  break	
				fi
			done
		fi
		echo "$ebook"
		cp "$ebook" ../../ebooks/
		cd ..
	fi
done
