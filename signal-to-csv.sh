SIGNAL_HOME="/home/florent/snap/signal-desktop/current"

sigBase="${SIGNAL_HOME}/.config/Signal/";
key=$( /usr/bin/jq -r '."key"' ${sigBase}config.json );
db="${SIGNAL_HOME}/.config/Signal/sql/db.sqlite";
clearTextMsgs="${sigBase}backup-desktop.csv";

/usr/bin/sqlcipher -list -noheader "$db" "PRAGMA key = \"x'"$key"'\";select json from messages;" > "$clearTextMsgs";

